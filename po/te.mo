��    X      �     �      �     �     �     �     �     �  	   �     �     �               0     <     J     W     t     |     �  	   �     �     �     �     �     �     �     �      	     	     "	     .	     :	  	   F	     P	     U	     c	  ?   h	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   
     
     !
     0
     6
  
   B
     M
     U
     i
     �
     �
     �
     �
  #   �
     �
     �
            
   /     :  %   J  %   p     �     �     �     �     �     �  )   �     �       	        !     4     M     S     `     i     u     {     �     �     �  �  �     4     N  f   g  =   �  4        A     ]  B   d  C   �  A   �  .   -  ?   \  1   �  U   �     $     =  F   M     �  Z   �  U   �  )   U       $   �  W   �       =     A   ]  %   �     �     �     �       /   +  	   [  �   e  <   E     �     �     �     �  '   �  B   �  2   B     u  "   �  2   �  4   �       $   *     O  '   n  V   �  R   �  B   @  R   �     �  .   �  f   !  �   �       ;     [   [     �  ?   �  �     z   �       *   )     T     m     �  %   �  f   �  Z   &     �     �  F   �  U   �     N     c      �  #   �     �     �     �                   @          B         E   I   '              C   >       N      
             -      9       /   =       %                 X           O                         1       8         5   <   4      D       P   U   *      	       &   F   6   2       #                  ,   !       M   A   "                         (   3   S             )   G   K         +   Q   7   J       .            W   $       ;           T          R                       0   :   L   ?      V   H        A Device A _File A newer version is available Antivirus definitions Antivirus engine Automatic CD/DVD Cannot connect... Check failed Check for updates Checking... Clear _Output Close window Could not scan (permissions) Current Description Devices available Directory Directory excluded from scan Downloading updates... E_xit Email Exit Exit this program File Files Scanned:  Files Scanned: %d Floppy disk GUI updates GUI version Histories Home Home (_Quick) Hour If you have connected a device, you may need to mount it first. Manage _Histories Manual Minute N/A Name Never No devices were found. No file selected None None found Operation failed. Please wait... Port: Preferences Quarantine Restore Scan for viruses... Scan your home directory Scanning %s... Scanning Histories Select Select File Select a Directory (directory scan) Select a device or press Cancel Status Stop scanning now Submit a file for analysis USB device Unable to check Unable to create graphical interface! Unable to create startup directories! Unknown Updated Updates Version View Virus Scanner Your antivirus signatures are out-of-date Your preferences were saved. _About _Advanced _Check for updates _Empty Quarantine Folder _Help _Maintenance _Options _Quarantine _Scan _Status _View file files Project-Id-Version: clamtk
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-05-18 06:56-0500
PO-Revision-Date: 2012-04-30 04:38+0000
Last-Translator: Praveen Illa <mail2ipn@gmail.com>
Language-Team: Telugu <te@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2012-05-23 13:01+0000
X-Generator: Launchpad (build 15282)
 ఒక పరికరం ఒక ఫైల్ (_F) ఒక కొత్త రూపాంతరము అందుబాటులోవున్నది యాంటీవైరస్ నిర్వచనాలు యాంటీవైరస్ యంత్రము స్వయంచాలక CD/DVD అనసంధానమవ్వలేకపోతుంది... పరిశీలించుటలో విఫలమైంది నవీకరణల కోసం పరిశీలించు తనిఖీ చేస్తోంది... ఉత్పాదనను చెరిపివేయి (_O) కిటికీని మూసివేయి స్కాన్ చేయలేకపోతుంది (అనుమతులు) ప్రస్తుత వివరణ అందుబాటులోవున్న పరికరాలు సంచయం స్కాన్ చేయకుండా వదిలివేసిన సంచయం నవీకరణలను దిగుమతిచేస్తున్నది... నిష్క్రమించు (_x) ఇమెయిల్ నిష్క్రమించు ఈ ప్రోగ్రాము నుండి నిష్క్రమించు ఫైల్ స్కాన్ చేయబడిన ఫైళ్ళు:  స్కాన్‌చేయబడిన ఫైళ్ళు: %d ఫ్లాపీ డిస్క్ GUI నవీకరణలు GUI రూపాంతరం చరిత్రలు నివాసం నివాసం (త్వరితం) (_Q) గంట ఒకవేళ మీరు ఒక పరికరముని అనుసంధానించినట్లయితే, మొదట దానిని మౌంట్ చేయాల్సివుంటుంది. చరిత్రను నిర్వహించు (_H) కరదీపిక నిముషము వర్తించదు పేరు ఎప్పటికీవద్దు ఏ పరికరాలు కనుగొనబడలేదు. ఏ ఫైలు ఎంపికకాలేదు ఏదీకాదు ఏమీ కనపడలేదు ఆపరేషన్ విఫలమైంది. దయచేసి వేచిఉండండి... పోర్టు: ప్రాధాన్యతలు క్వరైంటైన్ పునరుద్ధరించు వైరస్సుల కోసం సంశోధిస్తున్నది... మీ నివాస సంచయాన్ని స్కాన్ చేయి %s స్కానింగుచేస్తున్నది... చరిత్రలను స్కాన్‌చేస్తున్నది ఎంచుకోండి ఫైలుని ఎంచుకోండి ఒక సంచయాన్ని ఎంచుకోండి (సంచయపు స్కాన్) ఒక పరికరాన్ని ఎంచుకోండి లేదా రద్దుచేయి నొక్కండి స్థితి స్కానింగును ఇపుడు ఆపు ఒక ఫైలును విశ్లేషణ కోసం దాఖలుచేయి USB పరికరం పరిశీలించలేకపోతున్నది చిత్రరూపాల అంతరవర్తిని సృష్టించుట సాధ్యపడుట లేదు! ప్రారంభక సంచయాలను సృష్టించుట సాధ్యపడుట లేదు! తెలియదు నవీకరించబడింది నవీకరణలు రూపాంతరం వీక్షణ వైరస్ స్కానర్ మీ యాంటీవైరస్ చిహ్నాలు కాలంచెల్లినవి మీ ప్రాధాన్యతలు భద్రపరుచబడ్డాయి. గురించి (_A) ఉన్నత (_A) నవీకరణల కోసం పరిశీలించు (_C) క్వరంటైన్ సంచయాన్ని ఖాళీచేయి (_E) సహాయం (_H) నిర్వాహణ (_M) ఐచ్ఛికాలు (_O) క్వరైంటైన్ (_Q) స్కాన్ (_S) స్థితి (_S) వీక్షణ (_V) ఫైల్ ఫైళ్ళు 