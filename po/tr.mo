��    :      �  O   �      �  (   �     "     (     5     >     U     g     ~  2   �     �  9   �       	        %     :     Z     _     d     r     z     �     �     �     �  
   �     �     �     �     �     �     �  1        9  $   V     {     �  1   �     �     �     �                 %   ,     R     Z  (   p     �     �     �  
   �     �  	   �     �     �     �     �  �   	  )   �
  	   �
     �
  
   �
     �
     �
          '  ,   @     m  X   s     �     �     �  .   �     !     '     -     ;     D     H     e     i     }  	   �     �     �     �     �     �     �  /   �  (   !  (   J  !   s     �  /   �     �  	   �     �     �               (  
   B     M  <   c     �  
   �  !   �     �     �     �                 	            3   )       .      *         -       +      :   5      1                  9   4   !      7                        $       
              (   8   ,                            "   /   '      0   %       6                                #      &             2   	                     
Found %d possible %s (%d %s scanned).

 About Action Taken Advanced An update is available Check for updates ClamAV Signatures: %d
 ClamTk Virus Scanner ClamTk is a graphical front-end for Clam Antivirus Date Detect packed binaries, password recovery tools, and more Directories Scanned:
 Directory Environment settings Error updating: try again later Exit File File Analysis History Never No threats found.
 None Please wait... Problems opening %s... Quarantine Quit Restore Result Save results Scan a directory Scan a file Scan all files and directories within a directory Scan directories recursively Scan files beginning with a dot (.*) Scan files larger than 20 MB Scan for threats... Scan large files which are typically not examined Scanning %s... Schedule Set manually Settings Status Submit file for analysis The antivirus signatures are outdated Unknown Updates are available Uploaded files must be smaller than 32MB Vendor View View and set your preferences Viewing %s Virus Scanner Whitelist file files threat threats Project-Id-Version: 6.02
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-04-16 02:50-0500
PO-Revision-Date: 2019-08-05 11:09+0000
Last-Translator: Butterfly <Unknown>
Language: Turkish
Language-Team: Turkish <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-09-14 11:05+0000
X-Generator: Launchpad (build 19048)
 
%d olası %s bulundu (%d %s tarandı).

 Hakkında Uygulanan Davranış Gelişmiş Bir güncelleme mevcut Güncellemeleri kontrol et ClamAV İmzaları: %d
 ClamTk Virüs Tarayıcı ClamTk, Clam Antivirus İçin Grafik Arayüz Tarih Paketlenmiş ikili kodları, parola kurtarma araçlarını ve daha fazlasını tespit et Taranan Dizin:
 Dizin Ortam ayarları Güncelleme hatası: daha sonra tekrar deneyin Çık Dosya Dosya Analizi Geçmiş Yok Hiç bir tehdit bulunmadı.
 Yok Lütfen bekleyin... %s açılırken hata oluştu... Karantina Kapat Geri Yükle Sonuç Sonuçları kaydet Bir dizin tara Bir dosya tara Dizinin içindeki tüm dizin ve dosyaları tara Dizinleri alt dizinleriyle birlikte tara Nokta ile başlayan (.*) dosyaları tara 20 MB'dan büyük dosyaları tara Tehditleri tara... Genellikle incelenmeyen büyük dosyaları tara %s taranıyor... Zamanlama Elle ayarla Ayarlar Durum Analiz için dosya gönder Antivirüs imzaları eski Bilinmiyor Güncellemeler mevcut Karşıya yüklenen dosya boyutu 32 MB.'dan küçük olmalı Sağlayıcı Görünüm Tercihleri görüntüle ve ayarla %s inceleniyor Virüs Tarayıcı Beyaz Liste dosya dosya tehdit tehditler 