��    -      �  =   �      �  (   �     
                2     I  9   N     �  	   �     �     �     �     �     �     �     �     �     �     
  
   !     ,     4     ;     C  1   P     �  $   �     �  1   �          "     0     =  (   D     m     u     |  
   �     �  	   �     �     �     �     �  �  �  /   Z     �     �     �     �     �  A   �     	  
   (	     3	     G	     M	     V	     g	     p	     w	     �	     �	     �	  	   �	     �	     �	  	   �	     �	  5    
      6
  ,   W
      �
  6   �
     �
     �
             !     	   8     B     P     W     g     u     �     �  	   �  	   �     -               *   )         	                                         (   !   ,   "                                                                         $      &   %      #             +          '      
    
Found %d possible %s (%d %s scanned).

 Action Taken Analysis Check for updates ClamAV Signatures: %d
 Date Detect packed binaries, password recovery tools, and more Directories Scanned:
 Directory Environment settings Exit File File Analysis History Never No threats found.
 None Please wait... Problems opening %s... Quarantine Restore Result Results Save results Scan all files and directories within a directory Scan directories recursively Scan files beginning with a dot (.*) Scan files larger than 20 MB Scan large files which are typically not examined Scanning %s... Select a file Set manually Status The antivirus signatures are out-of-date Unknown Vendor View Viewing %s Virus Scanner Whitelist file files threat threats Project-Id-Version: clamtk
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-02-07 04:04-0600
PO-Revision-Date: 2015-02-23 12:42+0000
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-03-05 22:48+0000
X-Generator: Launchpad (build 17374)
 
Pronađeno %d mogućih %s (%d %s skenirano).

 Poduzete radnje Analiza Potraži nadopune ClamAV potpisi: %d
 Datum Otkrij upakirane binarne datoteke, alate za sanaciju lozinke itd. Skenirano direktorija:
 Direktorij Postavke okruženja Izlaz Datoteka Analiza datoteke Povijest Nikada Nema pronađenih prijetnji.
 Ništa Molim pričekajte... Problemi pri otvaranju %s... Karantena Vrati Rezultat Rezultati Spremi razultat Pregledaj sve datoteke i poddirektorije u direktoriju Skenirjte direktorije rekurzivno Skeniraj datoteke koje počinju točkom (.*) Skeniraj datoteke veće od 20 MB Skeniranje velikih datoteka koje obično nisu ispitane Skeniranje %s... Odaberite datoteku Postavi ručno Stanje Antivirusni potpisi su zastarjeli Nepoznato Isporučitelj Prikaz Prikazivanje %s Skener virusa Bijela lista datoteka datoteke prijetnja prijetnje 