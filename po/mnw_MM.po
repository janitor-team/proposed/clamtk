# Mon translation for clamtk
# Copyright (C) 2004-2020, Dave M <dave.nerd@gmail>
# Copyright (c) 2020 Rosetta Contributors and Canonical Ltd 2020
# This file is distributed under the same license as the clamtk package.
# Tao Mon Lae, 2020
#
msgid ""
msgstr ""
"Project-Id-Version: clamtk\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2020-08-05 05:26-0500\n"
"PO-Revision-Date: 2020-08-11 12:35+0000\n"
"Last-Translator: Tao Mon Lae <Unknown>\n"
"Language-Team: Mon <mnw@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-08-18 07:20+0000\n"
"X-Generator: Launchpad (build d6d0b96812d8def2ca0ffcc25cb4d200f2f30aeb)\n"
"Language: Mon"

#: lib/Analysis.pm:66 lib/GUI.pm:456 lib/GUI.pm:536 lib/GUI.pm:677
#: lib/Results.pm:156 lib/Results.pm:231
msgid "Analysis"
msgstr "သ္ၚေဝ်ဂၠိပ်ပါ်ပါဲ"

#: lib/Analysis.pm:78 lib/Schedule.pm:66
msgid "ClamTk Virus Scanner"
msgstr "စက်ဂၠာဲစၟ CLamTk"

#: lib/Analysis.pm:83
msgid ""
"This section allows you to check the status of a file from Virustotal. If a "
"file has been previously submitted, you will see the results of the scans "
"from dozens of antivirus and security vendors. This allows you to make a "
"more informed decision about the safety of a file.\n"
"\n"
"If the file has never been submitted to Virustotal, you can submit it "
"yourself. When you resubmit the file minutes later, you should see the "
"results.\n"
"\n"
"Please note you should never submit sensitive files to Virustotal.  Any "
"uploaded file can be viewed by Virustotal customers. Do not submit anything "
"with personal information or passwords.\n"
"\n"
"https://www.virustotal.com/gui/home/search"
msgstr ""

#: lib/Analysis.pm:93
msgid "Help"
msgstr "အရီုအဗၚ်"

#: lib/Analysis.pm:96
msgid "What is this?"
msgstr "ဣဏအ်ဂှ်မုမုရော။"

#: lib/Analysis.pm:111
msgid "File Analysis"
msgstr "သ္ၚေဝ်ဂၠိပ်ပါ်ပါဲ ဝှါၚ်"

#: lib/Analysis.pm:118 lib/Results.pm:36 lib/Results.pm:41
msgid "Results"
msgstr "သွဟ်ဂမၠိုၚ်"

#: lib/Analysis.pm:127 lib/History.pm:209 lib/Results.pm:169
#: lib/Schedule.pm:193 lib/Schedule.pm:242
msgid "Close"
msgstr "ကၟာတ်"

#: lib/Analysis.pm:184
msgid "Vendor"
msgstr "ညးသွံရာန်"

#: lib/Analysis.pm:193
msgid "Date"
msgstr "စၟတ်တ္ၚဲ"

#: lib/Analysis.pm:202
msgid "Result"
msgstr "သွဟ်"

#: lib/Analysis.pm:222
msgid "Save results"
msgstr "ဂိုၚ်ဒေပ်သွဟ်"

#: lib/Analysis.pm:240
msgid "Check or recheck a file's reputation"
msgstr ""

#: lib/Analysis.pm:259 lib/GUI.pm:576 lib/Shortcuts.pm:41
msgid "Select a file"
msgstr "ရုဲကေတ်ဝှါၚ်မွဲ"

#: lib/Analysis.pm:281
msgid "Submit file for analysis"
msgstr "ပ္တိုန်ဝှါၚ်သွက်ဂွံသ္ၚေဝ်ဂၠိပ်ပါ်ပါဲ"

#: lib/Analysis.pm:296
msgid "Uploaded files must be smaller than 32MB"
msgstr "လျိုၚ်ဝှါၚ်ဂွံပ္တိုန်တံဂှ် သ္ဒးဍောတ်နူ ၃၂ MB ရောၚ်"

#: lib/Analysis.pm:312
msgid ""
"No information exists for this file. Press OK to submit this file for "
"analysis."
msgstr "တၚ်နၚ်သွက်ဝှါၚ်ဏံဟွံမွဲရောၚ်၊ သွက်ဂွံပ္တိုန် ဝှါၚ်ဏံဂှ် ဍဵု OK ညိ။"

#: lib/Analysis.pm:323 lib/Analysis.pm:639
msgid "Unable to submit file: try again later"
msgstr "ဝှၚ်ဏံပ္တိုန်ဟွံဂွံ၊ ဆက်ဂ္စာန်အာပၠန်ညိ"

#: lib/Analysis.pm:336
msgid "View or delete previous results"
msgstr "ပံက်ဗဵု (ဝါ) ဇိုတ်ပလီုထောံသွဟ်မတုဲကၠုၚ်တေံညိ"

#: lib/Analysis.pm:363
msgid "View file results"
msgstr "ပံက်ဗဵုသွဟ်ဝှါၚ်"

#: lib/Analysis.pm:428
msgid "Delete file results"
msgstr "ဇိုတ်ပလီုသွဟ်ဝှါၚ်"

#: lib/Analysis.pm:501 lib/Analysis.pm:595 lib/Assistant.pm:100 lib/Scan.pm:102
#: lib/Update.pm:154
msgid "Please wait..."
msgstr "မၚ်မွဲလစုတ်ညိ..."

#: lib/Analysis.pm:637
msgid "File successfully submitted for analysis."
msgstr "ဝှါၚ်သွက်ဂွံသ္ၚေဝ်ဂၠိပ်ပါ်ပါဲ ဂှ် ပ္တိုန်ဏာဗွဲမအံၚ်ဇၞးရ"

#: lib/Analysis.pm:697 lib/Analysis.pm:702
msgid "Unable to save file"
msgstr "ဂိုၚ်ဒေပ်ဝှါၚ်ဟွံဂွံ"

#: lib/Analysis.pm:711
msgid "File has been saved"
msgstr "ဝှါၚ်ဏံသီဂိုၚ်တုဲဒှ်"

#: lib/App.pm:246
msgid "Unknown"
msgstr "ဟွံတီ"

#: lib/App.pm:397 lib/Prefs.pm:176
msgid "Never"
msgstr "ဆလအ်မှ"

#: lib/App.pm:455
msgid "Scan for threats..."
msgstr "စိစောတ်ဂၠိုက်ဂၠာဲအန္တရာယ်"

#: lib/App.pm:456
msgid "Advanced"
msgstr "ကဆံၚ်သၠုၚ်"

#: lib/App.pm:457 lib/GUI.pm:430 lib/GUI.pm:431 lib/GUI.pm:539
msgid "Scan a file"
msgstr "စိစောတ်ဂၠိုက်ဂၠာဲဝှါၚ်မွဲ"

#: lib/App.pm:458 lib/GUI.pm:435 lib/GUI.pm:436 lib/GUI.pm:542
msgid "Scan a directory"
msgstr "စိစောတ်ဂၠိုက်ဂၠာဲ ဍာန်ဝှါၚ်"

#: lib/App.pm:460 lib/GUI.pm:718
msgid "ClamTk is a graphical front-end for Clam Antivirus"
msgstr ""

#: lib/Assistant.pm:38
msgid "Please choose how you will update your antivirus signatures"
msgstr "ရုဲကေတ်ဗီုလဵုမၞး ကြက်သ္ဒး သၠုၚ်ပ္တိုန် သၞောတ်စဵုဒၞါစၟ"

#: lib/Assistant.pm:54
msgid "My computer automatically receives updates"
msgstr "စက်အဲ ဂွံဒုၚ်သၠုၚ်ပ္တိုန် Updates နအလိုက်ဍေံကုဍေံ"

#: lib/Assistant.pm:68
msgid "I would like to update signatures myself"
msgstr "အဲမိက်ဂွံ Update သၞောတ်ကေတ်တ်"

#: lib/Assistant.pm:93 lib/Assistant.pm:150 lib/Network.pm:110
msgid "Press Apply to save changes"
msgstr ""

#: lib/Assistant.pm:122
msgid "Your changes were saved."
msgstr "အရာပြံၚ်သၠာဲလဝ်ဂှ် ဂိုၚ်ဒေပ်လဝ်တုဲရ"

#: lib/Assistant.pm:125 lib/Update.pm:180 lib/Update.pm:186
msgid "Error updating: try again later"
msgstr ""

#: lib/GUI.pm:69
msgid "Virus Scanner"
msgstr "အရာမဂၠိုက်ဂၠာဲ စၟဳစၟတ်စၟ"

#: lib/GUI.pm:81 lib/GUI.pm:533 lib/Shortcuts.pm:29
msgid "About"
msgstr "ပရုပရာ"

#: lib/GUI.pm:127
msgid "Updates are available"
msgstr ""

#: lib/GUI.pm:130
msgid "The antivirus signatures are outdated"
msgstr ""

#: lib/GUI.pm:133
msgid "An update is available"
msgstr ""

#: lib/GUI.pm:217 lib/GUI.pm:555
msgid "Settings"
msgstr ""

#: lib/GUI.pm:218
msgid "View and set your preferences"
msgstr ""

#: lib/GUI.pm:222 lib/GUI.pm:563
msgid "Whitelist"
msgstr ""

#: lib/GUI.pm:223
msgid "View or update scanning whitelist"
msgstr ""

#: lib/GUI.pm:227 lib/GUI.pm:561
msgid "Network"
msgstr ""

#: lib/GUI.pm:228
msgid "Edit proxy settings"
msgstr ""

#: lib/GUI.pm:232 lib/GUI.pm:545 lib/Schedule.pm:54
msgid "Scheduler"
msgstr ""

#: lib/GUI.pm:233
msgid "Schedule a scan or signature update"
msgstr ""

#: lib/GUI.pm:295 lib/GUI.pm:557
msgid "Update"
msgstr ""

#: lib/GUI.pm:296
msgid "Update antivirus signatures"
msgstr ""

#: lib/GUI.pm:300 lib/GUI.pm:565
msgid "Update Assistant"
msgstr ""

#: lib/GUI.pm:301
msgid "Signature update preferences"
msgstr ""

#: lib/GUI.pm:363 lib/GUI.pm:553 lib/GUI.pm:665 lib/History.pm:41
msgid "History"
msgstr ""

#: lib/GUI.pm:364
msgid "View previous scans"
msgstr ""

#: lib/GUI.pm:368 lib/GUI.pm:559 lib/Results.pm:139 lib/Results.pm:216
msgid "Quarantine"
msgstr ""

#: lib/GUI.pm:369
msgid "Manage quarantined files"
msgstr ""

#: lib/GUI.pm:457
msgid "View a file's reputation"
msgstr ""

#: lib/GUI.pm:599 lib/GUI.pm:637 lib/Scan.pm:777 lib/Scan.pm:788
msgid "You do not have permissions to scan that file or directory"
msgstr ""

#: lib/GUI.pm:613 lib/Shortcuts.pm:47 lib/Whitelist.pm:100
msgid "Select a directory"
msgstr ""

#: lib/GUI.pm:658
msgid "Configuration"
msgstr ""

#: lib/GUI.pm:671
msgid "Updates"
msgstr ""

#: lib/GUI.pm:709
msgid "Homepage"
msgstr ""

#: lib/History.pm:80
msgid "View"
msgstr ""

#: lib/History.pm:88
msgid "Print"
msgstr ""

#: lib/History.pm:105 lib/Quarantine.pm:98 lib/Results.pm:146
#: lib/Results.pm:224 lib/Schedule.pm:203
msgid "Delete"
msgstr ""

#: lib/History.pm:150
#, perl-format
msgid "Viewing %s"
msgstr ""

#: lib/History.pm:166
#, perl-format
msgid "Problems opening %s..."
msgstr ""

#: lib/Network.pm:41
msgid "No proxy"
msgstr ""

#: lib/Network.pm:46
msgid "Environment settings"
msgstr ""

#: lib/Network.pm:51
msgid "Set manually"
msgstr ""

#: lib/Network.pm:55
msgid "IP address or host"
msgstr ""

#: lib/Network.pm:76
msgid "Port"
msgstr ""

#: lib/Network.pm:263
msgid "Settings saved"
msgstr ""

#: lib/Network.pm:270
msgid "Error"
msgstr ""

#: lib/Quarantine.pm:56
msgid "Number"
msgstr ""

#: lib/Quarantine.pm:67 lib/Results.pm:78
msgid "File"
msgstr ""

#: lib/Quarantine.pm:85
msgid "Restore"
msgstr ""

#: lib/Quarantine.pm:124 lib/Results.pm:281
#, perl-format
msgid "Really delete this file (%s) ?"
msgstr ""

#: lib/Quarantine.pm:185
msgid "Save file as..."
msgstr ""

#: lib/Results.pm:89 lib/Schedule.pm:215
msgid "Status"
msgstr ""

#: lib/Results.pm:100
msgid "Action Taken"
msgstr ""

#: lib/Results.pm:219
msgid "Quarantined"
msgstr ""

#: lib/Results.pm:227
msgid "Deleted"
msgstr ""

#: lib/Scan.pm:141
msgid "Preparing..."
msgstr ""

#: lib/Scan.pm:152 lib/Scan.pm:415
#, perl-format
msgid "Files scanned: %d"
msgstr ""

#: lib/Scan.pm:156 lib/Scan.pm:486
#, perl-format
msgid "Possible threats: %d"
msgstr ""

#: lib/Scan.pm:398
#, perl-format
msgid "Scanning %s..."
msgstr ""

#: lib/Scan.pm:484
msgid "None"
msgstr ""

#: lib/Scan.pm:510
msgid "Cleaning up..."
msgstr ""

#: lib/Scan.pm:517 lib/Update.pm:224 lib/Update.pm:227
msgid "Complete"
msgstr ""

#: lib/Scan.pm:520
msgid "Scanning complete"
msgstr ""

#: lib/Scan.pm:522
msgid "Possible threats found"
msgstr ""

#: lib/Scan.pm:564
msgid "No threats found"
msgstr ""

#: lib/Scan.pm:601
#, perl-format
msgid "ClamAV Signatures: %d\n"
msgstr ""

#: lib/Scan.pm:603
msgid "Directories Scanned:\n"
msgstr ""

#: lib/Scan.pm:608
#, perl-format
msgid ""
"\n"
"Found %d possible %s (%d %s scanned).\n"
"\n"
msgstr ""

#: lib/Scan.pm:610
msgid "threat"
msgstr ""

#: lib/Scan.pm:610
msgid "threats"
msgstr ""

#: lib/Scan.pm:612
msgid "file"
msgstr ""

#: lib/Scan.pm:612
msgid "files"
msgstr ""

#: lib/Scan.pm:624
msgid "No threats found.\n"
msgstr ""

#: lib/Schedule.pm:81
msgid "Scan"
msgstr ""

#: lib/Schedule.pm:88
msgid "Select a time to scan your home directory"
msgstr ""

#: lib/Schedule.pm:92
msgid "Set the scan time using a 24 hour clock"
msgstr ""

#: lib/Schedule.pm:103 lib/Schedule.pm:163
msgid "Set the hour using a 24 hour clock"
msgstr ""

#: lib/Schedule.pm:104 lib/Schedule.pm:164
msgid "Hour"
msgstr ""

#: lib/Schedule.pm:111 lib/Schedule.pm:170
msgid "Minute"
msgstr ""

#: lib/Schedule.pm:146 lib/Update.pm:80 lib/Update.pm:205 lib/Update.pm:220
msgid "Antivirus signatures"
msgstr ""

#: lib/Schedule.pm:153
msgid "Select a time to update your signatures"
msgstr ""

#: lib/Schedule.pm:224 lib/Schedule.pm:288
msgid "A daily scan is scheduled"
msgstr ""

#: lib/Schedule.pm:229 lib/Schedule.pm:301
msgid "A daily definitions update is scheduled"
msgstr ""

#: lib/Schedule.pm:292
msgid "A daily scan is not scheduled"
msgstr ""

#: lib/Schedule.pm:306
msgid "A daily definitions update is not scheduled"
msgstr ""

#: lib/Schedule.pm:315
msgid "You are set to automatically receive updates"
msgstr ""

#: lib/Settings.pm:42
msgid "Scan for PUAs"
msgstr ""

#: lib/Settings.pm:44
msgid "Detect packed binaries, password recovery tools, and more"
msgstr ""

#: lib/Settings.pm:57
msgid "Use heuristic scanning"
msgstr ""

#: lib/Settings.pm:72
msgid "Scan files beginning with a dot (.*)"
msgstr ""

#: lib/Settings.pm:73
msgid "Scan files typically hidden from view"
msgstr ""

#: lib/Settings.pm:86
msgid "Scan files larger than 20 MB"
msgstr ""

#: lib/Settings.pm:88
msgid "Scan large files which are typically not examined"
msgstr ""

#: lib/Settings.pm:101
msgid "Scan directories recursively"
msgstr ""

#: lib/Settings.pm:103
msgid "Scan all files and directories within a directory"
msgstr ""

#: lib/Settings.pm:116
msgid "Check for updates to this program"
msgstr ""

#: lib/Settings.pm:118
msgid "Check online for application and signature updates"
msgstr ""

#: lib/Shortcuts.pm:35
msgid "Exit"
msgstr ""

#: lib/Update.pm:60
msgid "Product"
msgstr ""

#: lib/Update.pm:68
msgid "Installed"
msgstr ""

#: lib/Update.pm:105
msgid "You are configured to automatically receive updates"
msgstr ""

#: lib/Update.pm:108
msgid "Check for updates"
msgstr ""

#: lib/Update.pm:199
msgid "Downloading..."
msgstr ""

#: lib/Whitelist.pm:48
msgid "Directory"
msgstr ""

#: lib/Whitelist.pm:70
msgid "Add a directory"
msgstr ""

#: lib/Whitelist.pm:83
msgid "Remove a directory"
msgstr ""
