��    ,      |  ;   �      �  (   �     �     �               1  9   6     p  	   �     �     �     �     �     �     �     �     �     �  
   �                 1        O  $   l     �  1   �     �     �     �     
     !  (   (     Q     Y     `  
   e     p  	   ~     �     �     �     �  �  �  O   ?     �     �     �  %   �  
    	  �   	  1   �	     �	     �	  
   

     
  
   "
     -
  #   :
     ^
     e
  ?   s
     �
     �
     �
     �
  b      M   c  Q   �  6     Z   :  %   �      �     �  )   �  
     B   )     l     {     �     �     �     �     �     �     	              "   !         $   
          )             ,      +          &         %                                            *            	                          #      (                         '           
Found %d possible %s (%d %s scanned).

 Action Taken Analysis Check for updates ClamAV Signatures: %d
 Date Detect packed binaries, password recovery tools, and more Directories Scanned:
 Directory Environment settings Exit File History Never No threats found.
 None Please wait... Problems opening %s... Quarantine Restore Result Results Scan all files and directories within a directory Scan directories recursively Scan files beginning with a dot (.*) Scan files larger than 20 MB Scan large files which are typically not examined Scanning %s... Select a file Set manually Signatures are current Status The antivirus signatures are out-of-date Unknown Vendor View Viewing %s Virus Scanner Whitelist file files threat threats Project-Id-Version: clamtk 5.01
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-10-26 09:28-0500
PO-Revision-Date: 2013-11-18 02:44+0000
Last-Translator: Gheyret T.Kenji <Unknown>
Language-Team: Uyghur <ug@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-11-20 11:52+0000
X-Generator: Launchpad (build 16831)
 
%d بايقالدى مۇمكىنچىلىكى %s (%d %s تەكشۈرۈلدى)

 مەشغۇلات تاللاڭ تەھلىل يېڭىلاشنى تەكشۈر ClamAV ۋىرۇس ئامبىرى: %d
 چېسلا بوغچىلانغان ئىككىلىك ھۆججەتلەر، ئىم ئەسلىگە كەلتۈرۈش قوراللىرى ۋە باشقىلارنى بايقايدۇ تەكشۈرۈلگەن مۇندەرىجىلەر:
 مۇندەرىجە مۇھىت تەڭشەك چېكىن ھۆججەت تارىخ ھەرگىز تەھدىت بايقالمىدى.
 يوق كۈتۈڭ… %s نى ئېچىۋاتقاندا كاشىلا كۆرۈلدى… كارانتىن ئەسلىگە كەلتۈر نەتىجە نەتىجىلەر بىر مۇندەرىجىدىكى ھەممە ھۆججەت ۋە قىسقۇچلارنى تەكشۈر مۇندەرىجىلەرنى قايتىلانما ھالەتتە تەكشۈر چېكىت (.*) بىلەن باشلانغان ھۆججەتلەرنى تەكشۈر 20MB دىن چوڭ ھۆججەتلەرنى تەكشۈر ئادەتتە تەكشۈرۈلمەيدىغان چوڭ ھۆججەتلەرنى تەكشۈر %s نى تەكشۈرۈۋاتىدۇ… بىر ھۆججەت تاللاڭ قولدا تەڭشەك ۋىرۇس ئامبىرى ئەڭ يېڭى ھالەت بۇ ۋىرۇسخور ئىمزاسىنىڭ ۋاقتى ئۆتكەن نامەلۇم تەمىنلىگۈچى كۆرۈنۈش %s كۆرسىتىۋاتىدۇ ۋىرۇس تەكشۈرگۈچ ئاق تىزىم ھۆججەت ھۆججەت تەھدىت تەھدىت 